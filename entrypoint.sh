#!/bin/bash
mkdir /opt/themes

git clone $HUGO_THEME_URL /opt/themes/timer-hugo

hugo --themesDir /opt/themes -d /output
